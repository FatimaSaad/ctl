#ifndef CTL_META_CTL_NLOPT_H
#define CTL_META_CTL_NLOPT_H

/*
 * This header includes all headers of the CTL NLopt module
 * provided that all submodules are included in the qmake project, usually by including
 * the CTL NLopt module 'ctl_nlopt.pri'.
 * Otherwise, only the headers of the added submodules will be included.
 */

#ifdef REGIST_2D3D_MODULE_AVAILABLE
#include "registration/grangeatregistration2d3d.h"
#include "registration/projectionregistration2d3d.h"
#endif // REGIST_2D3D_MODULE_AVAILABLE

#endif // CTL_META_CTL_NLOPT_H
