#ifndef CTL_TRAJECTORIES_H
#define CTL_TRAJECTORIES_H

#include "abstractpreparestep.h"
#include "acquisitionsetup.h"
#include "preparesteps.h"
#include "mat/mat.h"

namespace CTL {
namespace protocols {

class HelicalTrajectory : public AbstractPreparationProtocol
{
    public:std::vector<std::shared_ptr<AbstractPrepareStep>>
           prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    HelicalTrajectory(double angleIncrement,
                      double pitchIncrement = 0.0,
                      double startPitch = 0.0,
                      double startAngle = 0.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    void setAngleIncrement(double angleIncrement);
    void setPitchIncrement(double pitchIncrement);
    void setStartPitch(double startPitch);
    void setStartAngle(double startAngle);

private:
    double _angleIncrement = 0.0;
    double _pitchIncrement = 0.0;
    double _startPitch     = 0.0;
    double _startAngle     = 0.0;
};

class WobbleTrajectory : public AbstractPreparationProtocol
{
    public:std::vector<std::shared_ptr<AbstractPrepareStep>>
           prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    WobbleTrajectory(double angleSpan,
                     double sourceToIsocenter,
                     double startAngle  = 0.0_deg,
                     double wobbleAngle = 15.0_deg,
                     double wobbleFreq  = 1.0);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    void setStartAngle(double startAngle);
    void setWobbleAngle(double wobbleAngle);
    void setWobbleFreq(double wobbleFreq);

private:
    double _angleSpan         = 0.0;
    double _sourceToIsocenter = 0.0;
    double _startAngle        = 0.0;
    double _wobbleAngle       = 0.0;
    double _wobbleFreq        = 0.0;
};

class CirclePlusLineTrajectory : public AbstractPreparationProtocol
{
    public:std::vector<std::shared_ptr<AbstractPrepareStep>>
           prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    CirclePlusLineTrajectory(double angleSpan,
                             double sourceToIsocenter,
                             double lineLength,
                             double fractionOfViewsForLine = 0.5,
                             double startAngle = 0.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

private:
    double _angleSpan              = 0.0;
    double _sourceToIsocenter      = 0.0;
    double _lineLength             = 0.0;
    double _fractionOfViewsForLine = 0.5;
    double _startAngle             = 0.0;
};

class ShortScanTrajectory : public AbstractPreparationProtocol
{
    public:std::vector<std::shared_ptr<AbstractPrepareStep>>
           prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    ShortScanTrajectory(double sourceToIsocenter,
                        double startAngle = 0.0_deg,
                        double angleSpan = -1.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

private:
    double _sourceToIsocenter = 0.0;
    double _startAngle        = 0.0;
    double _angleSpan         = -1.0;

    static double fanAngle(const AcquisitionSetup& setup);
};

class AxialScanTrajectory : public AbstractPreparationProtocol
{
    public:std::vector<std::shared_ptr<AbstractPrepareStep>>
           prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    AxialScanTrajectory(double startAngle = 0.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

    void setStartAngle(double startAngle);

private:
    double _startAngle = 0.0;
};

class TomosynthesisCircleTrajectory : public AbstractPreparationProtocol
{
    public:std::vector<std::shared_ptr<AbstractPrepareStep>>
           prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisCircleTrajectory(double sourceToIsocenter,
                                  double tomoAngle = 15.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

private:
    double _sourceToIsocenter = 0.0;
    double _tomoAngle         = 0.0;
};

template<uint nbArcs>
class TomosynthesisMultiArcTrajectory : public AbstractPreparationProtocol
{
    static_assert (nbArcs > 1, "Multi arc trajectory must have nb. arcs larger than 1.");

    public:std::vector<std::shared_ptr<AbstractPrepareStep>>
           prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisMultiArcTrajectory(double sourceToIsocenter,
                                    double tomoAngle = 15.0_deg,
                                    double startAngle = 0.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

private:
    double _sourceToIsocenter = 0.0;
    double _startAngle        = 0.0;
    double _tomoAngle         = 15.0_deg;
};

template<>
class TomosynthesisMultiArcTrajectory<2> : public AbstractPreparationProtocol
{
    public:std::vector<std::shared_ptr<AbstractPrepareStep>>
           prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisMultiArcTrajectory(double sourceToIsocenter,
                                    double tomoAngle = 15.0_deg,
                                    double startAngle = 0.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

private:
    double _sourceToIsocenter = 0.0;
    double _startAngle        = 0.0;
    double _tomoAngle         = 15.0_deg;
};

using WedgeTrajectory                 = TomosynthesisMultiArcTrajectory<2>;
using TomosynthesisTriangleTrajectory = TomosynthesisMultiArcTrajectory<3>;
using TomosynthesisSaddleTrajectory   = TomosynthesisMultiArcTrajectory<4>;

// Template implementations
// ========================

template<uint nbArcs>
TomosynthesisMultiArcTrajectory<nbArcs>::TomosynthesisMultiArcTrajectory(double sourceToIsocenter,
                                                                         double tomoAngle,
                                                                         double startAngle)
    : _sourceToIsocenter(sourceToIsocenter)
    , _startAngle(startAngle)
    , _tomoAngle(tomoAngle)
{
}

template<uint nbArcs>
std::vector<std::shared_ptr<AbstractPrepareStep>>
TomosynthesisMultiArcTrajectory<nbArcs>::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    const auto viewsPerArc = setup.nbViews() / static_cast<double>(nbArcs);
    constexpr auto innerAngle = 2.0 * PI / double(nbArcs);
    const auto arcFactor = std::sqrt((1.0 - std::cos(2.0 * PI / nbArcs)) /
                                     (1.0 + std::cos(2.0 * PI / nbArcs)));
    const auto arcAngle = 2.0 * std::atan(arcFactor * std::sin(_tomoAngle));
    const auto angleIncrement = arcAngle / viewsPerArc;

    auto arcRot = [](double xAngle, double yAngle) {
        return mat::rotationMatrix(xAngle - PI_2, Qt::XAxis) *
               mat::rotationMatrix(yAngle, Qt::YAxis);
    };

    const auto rotAngle = std::fmod(viewNb, viewsPerArc) * angleIncrement - 0.5 * arcAngle;
    const auto arcNb = std::floor(viewNb / viewsPerArc);

    const auto viewRotation = mat::rotationMatrix(-arcNb * innerAngle + _startAngle, Qt::YAxis) *
                              arcRot(-_tomoAngle, rotAngle);

    const auto viewPosition = viewRotation * Vector3x1(0.0, 0.0, -_sourceToIsocenter);

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));

    return ret;
}

template<uint nbArcs>
bool TomosynthesisMultiArcTrajectory<nbArcs>::isApplicableTo(const AcquisitionSetup& setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

#if __cplusplus >= 201703L
class TomosynthesisEllipticalTrajectory : public AbstractPreparationProtocol
{
    public:std::vector<std::shared_ptr<AbstractPrepareStep>>
           prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisEllipticalTrajectory(double sourceToIsocenter,
                                      double startAngle,
                                      double smallAngle,
                                      double largeAngle,
                                      bool landscapeDetectorOrientation = false);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

private:
    double _sourceToIsocenter     = 0.0;
    double _startAngle            = 0.0;
    double _smallAngle            = 0.0;
    double _largeAngle            = 0.0;
    bool _landscapeDetOrientation = false;

    double computeAngularEllipsisParameter(uint viewNb, uint nbViews) const;
};
#endif // C++17

class TomosynthesisCrossOrbitTrajectory : public AbstractPreparationProtocol
{
    public:std::vector<std::shared_ptr<AbstractPrepareStep>>
           prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisCrossOrbitTrajectory(double sourceToIsocenter,
                        double startAngle = 0.0_deg,
                        double smallAngle = -1.0_deg,
                        double largeAngle = -1.0_deg,
                        double fractionOfViewsHorizontal = 0.5);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

private:
    double _sourceToIsocenter = 0.0;
    double _startAngle        = 0.0;
    double _smallAngle        = -1.0;
    double _largeAngle        = -1.0;
    double _fractionOfViewsHorizontal = 0.5;

};

class TomosynthesisVerticalTrajectory : public AbstractPreparationProtocol
{
    public:std::vector<std::shared_ptr<AbstractPrepareStep>>
           prepareSteps(uint viewNb, const AcquisitionSetup& setup) const override;

public:
    TomosynthesisVerticalTrajectory(double sourceToIsocenter,
                        double startAngle = 0.0_deg,
                        double tomoAngle = -1.0_deg);

    bool isApplicableTo(const AcquisitionSetup& setup) const override;

private:
    double _sourceToIsocenter = 0.0;
    double _startAngle        = 0.0;
    double _tomoAngle         = -1.0;

};

} // namespace protocols
} // namespace CTL

#endif // CTL_TRAJECTORIES_H
