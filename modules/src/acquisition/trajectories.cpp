#include "trajectories.h"
#include "components/carmgantry.h"
#include "components/cylindricaldetector.h"
#include "components/flatpaneldetector.h"

namespace CTL {
namespace protocols {

HelicalTrajectory::HelicalTrajectory(double angleIncrement,
                                     double pitchIncrement,
                                     double startPitch,
                                     double startAngle)
    : _angleIncrement(angleIncrement)
    , _pitchIncrement(pitchIncrement)
    , _startPitch(startPitch)
    , _startAngle(startAngle)
{
}

void HelicalTrajectory::setAngleIncrement(double angleIncrement) { _angleIncrement = angleIncrement; }

void HelicalTrajectory::setPitchIncrement(double pitchIncrement) { _pitchIncrement = pitchIncrement; }

void HelicalTrajectory::setStartPitch(double startPitch) { _startPitch = startPitch; }

void HelicalTrajectory::setStartAngle(double startAngle) { _startAngle = startAngle; }

std::vector<std::shared_ptr<AbstractPrepareStep>>
HelicalTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup&) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::TubularGantryParam>();

    gantryPrep->setPitchPosition(viewNb * _pitchIncrement + _startPitch);
    gantryPrep->setRotationAngle(viewNb * _angleIncrement + _startAngle);

    ret.push_back(std::move(gantryPrep));

    qDebug() << "HelicalTrajectory --- add prepare steps for view: " << viewNb
             << "\n-rotation: " << viewNb * _angleIncrement + _startAngle
             << "\n-pitch: " << viewNb * _pitchIncrement + _startPitch;

    return ret;
}

bool HelicalTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::TubularGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

WobbleTrajectory::WobbleTrajectory(double angleSpan,
                                   double sourceToIsocenter,
                                   double startAngle,
                                   double wobbleAngle,
                                   double wobbleFreq)
    : _angleSpan(angleSpan)
    , _sourceToIsocenter(sourceToIsocenter)
    , _startAngle(startAngle)
    , _wobbleAngle(wobbleAngle)
    , _wobbleFreq(wobbleFreq)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
WobbleTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();

    const uint nbViews = setup.nbViews();
    const Vector3x1 initialSrcPos(0.0, 0.0, -_sourceToIsocenter);
    const Matrix3x3 fixedRotMat = mat::rotationMatrix(PI_2 + _startAngle, Qt::ZAxis)
        * mat::rotationMatrix(-PI_2, Qt::XAxis);
    const double angleIncrement = (nbViews > 1) ? _angleSpan / double(nbViews - 1) : 0.0;

    const auto wobblePhase = std::sin(double(viewNb) / double(nbViews) * 2.0 * PI * _wobbleFreq);
    const auto wobbleRotMat = mat::rotationMatrix(wobblePhase * _wobbleAngle, Qt::XAxis);

    const auto viewRotation
        = mat::rotationMatrix(viewNb * angleIncrement, Qt::ZAxis) * fixedRotMat * wobbleRotMat;
    const auto viewPosition = viewRotation * initialSrcPos;

    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));

    ret.push_back(std::move(gantryPrep));

    return ret;
}

bool WobbleTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

void WobbleTrajectory::setStartAngle(double startAngle) { _startAngle = startAngle; }

void WobbleTrajectory::setWobbleAngle(double wobbleAngle) { _wobbleAngle = wobbleAngle; }

void WobbleTrajectory::setWobbleFreq(double wobbleFreq) { _wobbleFreq = wobbleFreq; }

CirclePlusLineTrajectory::CirclePlusLineTrajectory(double angleSpan,
                                                   double sourceToIsocenter,
                                                   double lineLength,
                                                   double fractionOfViewsForLine,
                                                   double startAngle)
    : _angleSpan(angleSpan)
    , _sourceToIsocenter(sourceToIsocenter)
    , _lineLength(lineLength)
    , _fractionOfViewsForLine(fractionOfViewsForLine)
    , _startAngle(startAngle)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
CirclePlusLineTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();

    const uint nbViews = setup.nbViews();
    const uint nbViewsLine   = static_cast<uint>(std::floor(nbViews * _fractionOfViewsForLine));
    const uint nbViewsCircle = nbViews - nbViewsLine;
    const Vector3x1 initialSrcPos(0.0, 0.0, -_sourceToIsocenter);
    const Matrix3x3 fixedRotMat = mat::rotationMatrix(PI_2 + _startAngle, Qt::ZAxis)
        * mat::rotationMatrix(-PI_2, Qt::XAxis);

    Vector3x1 viewPosition;
    Matrix3x3 viewRotation;

    if(viewNb < nbViewsCircle)
    {
        const double angleIncrement = (nbViewsCircle > 1) ? _angleSpan / double(nbViewsCircle - 1) : 0.0;

        viewRotation = mat::rotationMatrix(viewNb * angleIncrement, Qt::ZAxis) * fixedRotMat;
        viewPosition = viewRotation * initialSrcPos;
    }
    else
    {
        const uint lineViewNb = viewNb - nbViewsCircle;
        const double lineIncrement = (nbViewsLine > 1) ? _lineLength / double(nbViewsLine - 1) : 0.0;

        viewRotation = mat::rotationMatrix(_angleSpan/2.0, Qt::ZAxis) * fixedRotMat;
        viewPosition = viewRotation * initialSrcPos;
        viewPosition.get<2>() += (lineViewNb - nbViewsLine/2.0) * lineIncrement;
    }

    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));

    ret.push_back(std::move(gantryPrep));

    return ret;
}

bool CirclePlusLineTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

ShortScanTrajectory::ShortScanTrajectory(double sourceToIsocenter, double startAngle, double angleSpan)
    : _sourceToIsocenter(sourceToIsocenter)
    , _startAngle(startAngle)
    , _angleSpan(angleSpan)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep> > ShortScanTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup &setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();

    double angleSpan;
    if(_angleSpan >= 0.0)
        angleSpan = _angleSpan;
    else
        angleSpan = 180.0_deg + fanAngle(setup);

    qDebug() << "short scan angle span: " << angleSpan;

    const uint nbViews = setup.nbViews();
    const Vector3x1 initialSrcPos(0.0, 0.0, -_sourceToIsocenter);
    const Matrix3x3 fixedRotMat = mat::rotationMatrix(PI_2 + _startAngle, Qt::ZAxis)
        * mat::rotationMatrix(-PI_2, Qt::XAxis);
    const double angleIncrement = (nbViews > 1) ? angleSpan / double(nbViews - 1) : 0.0;

    const auto viewRotation = mat::rotationMatrix(viewNb * angleIncrement, Qt::ZAxis) * fixedRotMat;
    const auto viewPosition = viewRotation * initialSrcPos;

    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));

    ret.push_back(std::move(gantryPrep));

    return ret;
}

bool ShortScanTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

double ShortScanTrajectory::fanAngle(const AcquisitionSetup& setup)
{
    auto detector = setup.system()->detector();
    auto gantry   = static_cast<CarmGantry*>(setup.system()->gantry());

    double relevantWidth;
    switch (detector->type()) {
    case CylindricalDetector::Type:
    {
        const auto cylDetPtr = static_cast<CylindricalDetector*>(detector);
        const auto cylFan = cylDetPtr->fanAngle();
        relevantWidth = 2.0 * cylDetPtr->curvatureRadius() * std::sin(cylFan / 2.0);
        break;
    }
    case FlatPanelDetector::Type:
    {
        relevantWidth = static_cast<FlatPanelDetector*>(detector)->panelDimensions().width();
        break;
    }
    default:
        relevantWidth = 0.0;
        break;
    }

    return 2.0 * tan(0.5*relevantWidth / gantry->cArmSpan());
}

AxialScanTrajectory::AxialScanTrajectory(double startAngle)
    : _startAngle(startAngle)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
AxialScanTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    float angleIncrement = (setup.nbViews() > 0) ? 360.0_deg / setup.nbViews()
                                                 : 0.0;
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::TubularGantryParam>();

    gantryPrep->setRotationAngle(viewNb * angleIncrement + _startAngle);

    ret.push_back(std::move(gantryPrep));

    qDebug() << "AxialScanTrajectory --- add prepare steps for view: " << viewNb
             << "\n-rotation: " << viewNb * angleIncrement + _startAngle;

    return ret;
}

bool AxialScanTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::TubularGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

void AxialScanTrajectory::setStartAngle(double startAngle) { _startAngle = startAngle; }

TomosynthesisCircleTrajectory::TomosynthesisCircleTrajectory(double sourceToIsocenter,
                                                             double tomoAngle)
    : _sourceToIsocenter(sourceToIsocenter)
    , _tomoAngle(tomoAngle)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
TomosynthesisCircleTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    const auto nbViews = setup.nbViews();
    const auto fixedRotMat = mat::rotationMatrix(-PI_2 + _tomoAngle, Qt::XAxis);
    const auto angleIncrement = 360.0_deg / nbViews;

    const auto viewRotation = mat::rotationMatrix(viewNb * angleIncrement, Qt::YAxis) * fixedRotMat;
    const auto viewPosition = viewRotation * Vector3x1(0.0, 0.0, -_sourceToIsocenter);

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));

    return ret;
}

bool TomosynthesisCircleTrajectory::isApplicableTo(const AcquisitionSetup& setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}


TomosynthesisMultiArcTrajectory<2>::TomosynthesisMultiArcTrajectory(double sourceToIsocenter,
                                                                    double tomoAngle,
                                                                    double startAngle)
    : _sourceToIsocenter(sourceToIsocenter)
    , _startAngle(startAngle)
    , _tomoAngle(tomoAngle)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
TomosynthesisMultiArcTrajectory<2>::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    const auto viewsPerArc = setup.nbViews() / 2.0;
    constexpr auto arcAngle = PI;
    const auto angleIncrement = arcAngle / viewsPerArc;

    auto arcRot = [](double xAngle, double yAngle) {
        return mat::rotationMatrix(xAngle - PI_2, Qt::XAxis) *
               mat::rotationMatrix(yAngle, Qt::YAxis);
    };

    const auto rotAngle = std::fmod(viewNb, viewsPerArc) * angleIncrement - 0.5 * arcAngle;
    const auto arcNb = std::floor(viewNb / viewsPerArc);

    const Matrix3x3 viewRotation = mat::rotationMatrix(-arcNb * PI + _startAngle, Qt::YAxis) *
                                   arcRot(-_tomoAngle, rotAngle);

    const auto viewPosition = viewRotation * Vector3x1(0.0, 0.0, -_sourceToIsocenter);

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));

    return ret;
}

bool TomosynthesisMultiArcTrajectory<2>::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

#if __cplusplus >= 201703L
TomosynthesisEllipticalTrajectory::TomosynthesisEllipticalTrajectory(
    double sourceToIsocenter,
    double startAngle,
    double smallAngle,
    double largeAngle,
    bool landscapeDetectorOrientation)
    : _sourceToIsocenter(sourceToIsocenter)
    , _startAngle(startAngle)
    , _smallAngle(smallAngle)
    , _largeAngle(largeAngle)
    , _landscapeDetOrientation(landscapeDetectorOrientation)
{
    if(smallAngle > largeAngle)
        throw std::domain_error("TomosynthesisEllipticalTrajectory: "
                                "smallAngle must not be larger than largeAngle.");
}

std::vector<std::shared_ptr<AbstractPrepareStep>>
TomosynthesisEllipticalTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup& setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    const auto nbViews = setup.nbViews();

    // "portrait" detector orientation:
    auto fixedRotMat = mat::rotationMatrix(PI_2, Qt::XAxis);
    // "landscape" detector orientation:
    if(_landscapeDetOrientation)
        fixedRotMat = mat::rotationMatrix(PI_2, Qt::YAxis) * fixedRotMat;

    const auto smallRadius = _sourceToIsocenter * std::tan(_smallAngle);
    const auto largeRadius = _sourceToIsocenter * std::tan(_largeAngle);
    const auto angleParam = computeAngularEllipsisParameter(viewNb, nbViews);
    const auto locus = Vector3x1{ largeRadius * std::cos(angleParam),
                                  _sourceToIsocenter,
                                  smallRadius * std::sin(angleParam) };

    // construct rotation from Y-axis to locus
    const auto rotColumnY = locus.normalized();
    const auto rotColumnZ = mat::cross(locus, Vector3x1{ 0.0, 1.0, 0.0 }).normalized();
    const auto rotColumnX = mat::cross(rotColumnY, rotColumnZ);
    const auto rotEllips = mat::horzcat(mat::horzcat(rotColumnX, rotColumnY), rotColumnZ);

    auto viewRotation = rotEllips * fixedRotMat;
    viewRotation = mat::rotationMatrix(_startAngle, Qt::YAxis) * viewRotation;
    const auto viewPosition = viewRotation * Vector3x1(0.0, 0.0, -_sourceToIsocenter);

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));

    return ret;
}

double TomosynthesisEllipticalTrajectory::computeAngularEllipsisParameter(uint viewNb,
                                                                          uint nbViews) const
{
    const double m = 1 - std::pow(std::tan(_smallAngle) / std::tan(_largeAngle), 2);
    const double k = std::sqrt(m);
    const double perimeter = 4 * _sourceToIsocenter * std::tan(_largeAngle) * std::comp_ellint_2(k);
    const double arcLength = perimeter / nbViews;
    const double z = std::comp_ellint_2(k) - viewNb * arcLength
                     / (_sourceToIsocenter * std::tan(_largeAngle));
    const double ksi = 1 - z / std::ellint_2(k, PI_2);
    const double r = std::sqrt(std::pow(1 - m, 2) + std::pow(ksi, 2));
    const double theta = std::atan((1 - m) / ksi);

    double phi = PI_2 + std::sqrt(r) * (theta - PI_2);

    for(int i = 1; ; ++i)
    { 
        constexpr auto maxNewtonIterations = 300;
        constexpr auto tol = 1.0e-15;

        const double error = std::ellint_2(k, phi) - z;
        phi -= error / std::sqrt(1 - m * std::sin(phi) * std::sin(phi));

        qDebug() << "viewNb" << viewNb
                 << ": the inverse elliptic integral estimation error at iteration" << i
                 << "was" << error;

        // stopping criteria
        if(std::abs(error) < tol)
            break;
        if(i >= maxNewtonIterations)
        {
            qWarning("inverse elliptic integral estimation has not converged");
            break;
        }
    }
        
    auto eta = std::acos(std::sin(phi));
    if(viewNb > nbViews / 2)
        eta = 2 * PI - eta;
    return eta;
}

bool TomosynthesisEllipticalTrajectory::isApplicableTo(const AcquisitionSetup& setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}
#endif // C++17


TomosynthesisCrossOrbitTrajectory::TomosynthesisCrossOrbitTrajectory(double sourceToIsocenter, double startAngle, double smallAngle, double largeAngle, double fractionOfViewsHorizontal)
    : _sourceToIsocenter(sourceToIsocenter)
    , _startAngle(startAngle)
    , _smallAngle(smallAngle)
    , _largeAngle(largeAngle)
    , _fractionOfViewsHorizontal(fractionOfViewsHorizontal)
{
}

// For an equal sampling along the vertical and the horizontal orbits,
// choose fractionOfViewsHorizontal = ((nbViews - 1) * largeAngle + smallAngle) / (nbViews * (smallAngle + largeAngle));
std::vector<std::shared_ptr<AbstractPrepareStep> > TomosynthesisCrossOrbitTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup &setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    const uint nbViews = setup.nbViews();
    const uint nbViewsHorizontal   = static_cast<uint>(std::floor(nbViews * _fractionOfViewsHorizontal));
    const uint nbViewsVertical = nbViews - nbViewsHorizontal;
    const Vector3x1 initialSrcPos(0.0, 0.0, -_sourceToIsocenter);
    Vector3x1 viewPosition;
    Matrix3x3 viewRotation;

    if(viewNb < nbViewsVertical)
    {
        const Matrix3x3 fixedRotMat =  mat::rotationMatrix(-_smallAngle + PI_2, Qt::XAxis);
        const double angleIncrement = (nbViewsVertical > 1) ? 2*_smallAngle / double(nbViewsVertical - 1) : 0.0;
        viewRotation = mat::rotationMatrix(viewNb * angleIncrement, Qt::XAxis) * fixedRotMat;
        viewRotation = mat::rotationMatrix(_startAngle, Qt::YAxis) * viewRotation;
        viewPosition = viewRotation * initialSrcPos;
    }
    else
    {
        const uint horizontalViewNb = viewNb - nbViewsVertical;
        Matrix3x3 fixedRotMat = mat::rotationMatrix(-_largeAngle, Qt::ZAxis)
            * mat::rotationMatrix(PI_2, Qt::XAxis);
        const double angleIncrement = (nbViewsHorizontal > 1) ? 2*_largeAngle / double(nbViewsHorizontal - 1) : 0.0;
        viewRotation = mat::rotationMatrix(horizontalViewNb * angleIncrement, Qt::ZAxis) * fixedRotMat;
        viewRotation = mat::rotationMatrix(_startAngle, Qt::YAxis) * viewRotation;
        viewPosition = viewRotation * initialSrcPos;

    }

    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));
    return ret;
}

bool TomosynthesisCrossOrbitTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

TomosynthesisVerticalTrajectory::TomosynthesisVerticalTrajectory(double sourceToIsocenter, double startAngle, double tomoAngle)
    : _sourceToIsocenter(sourceToIsocenter)
    , _startAngle(startAngle)
    , _tomoAngle(tomoAngle)
{
}

std::vector<std::shared_ptr<AbstractPrepareStep> > TomosynthesisVerticalTrajectory::prepareSteps(uint viewNb, const AcquisitionSetup &setup) const
{
    std::vector<std::shared_ptr<AbstractPrepareStep>> ret;

    auto gantryPrep = std::make_shared<prepare::CarmGantryParam>();
    const uint nbViews = setup.nbViews();
    const Vector3x1 initialSrcPos(0.0, 0.0, -_sourceToIsocenter);
    const Matrix3x3 fixedRotMat = mat::rotationMatrix(-_startAngle - _tomoAngle + PI_2, Qt::XAxis);
    const double angleIncrement = (nbViews > 1) ? 2 * _tomoAngle / double(nbViews - 1) : 0.0;
    const auto viewRotation = mat::rotationMatrix(viewNb * angleIncrement, Qt::XAxis) * fixedRotMat;
    const auto viewPosition = viewRotation * initialSrcPos;

    gantryPrep->setLocation(mat::Location(viewPosition, viewRotation));
    ret.push_back(std::move(gantryPrep));
    return ret;
}


bool TomosynthesisVerticalTrajectory::isApplicableTo(const AcquisitionSetup &setup) const
{
    prepare::CarmGantryParam tmp;
    return tmp.isApplicableTo(*setup.system());
}

} // namespace protocols
} // namespace CTL
