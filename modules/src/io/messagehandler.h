#ifndef CTL_MESSAGEHANDLER_H
#define CTL_MESSAGEHANDLER_H

#include <QMessageLogContext>
#include <QObject>
#include <QStringList>

namespace CTL {

class MessageHandler : public QObject
{
    Q_OBJECT
public:
    // non-copyable
    MessageHandler(const MessageHandler&) = delete;
    MessageHandler(MessageHandler&&) = delete;
    MessageHandler& operator=(const MessageHandler&) = delete;
    MessageHandler& operator=(MessageHandler&&) = delete;
    ~MessageHandler() override = default;

    // get the instance of the message handler
    static MessageHandler& instance();

    // installer method to be used to set in qInstallMessageHandler()
    static void qInstaller(QtMsgType type, const QMessageLogContext& context, const QString &msg);

    // getter for full log
    const QStringList& log() const;

    // getter for last message (convenience)
    QString lastMessage() const;

    // blacklist everything
    void blacklistAll(bool blacklist = true);
    // add a class to the log blacklist
    template<class ... QString>
    void blacklistClassOrFunction(const QString& ... names);
    // add a file to the log blacklist
    template<class ... QString>
    void blacklistFile(const QString& ... names);
    // add a messsage type to the blacklist
    void blacklistMessageType(QtMsgType type);
    // clears all blacklists (every message will be processed)
    void clearAllBlacklists();
    // clears all whitelists
    void clearAllWhitelists();

    // enforce that messages appear in log even when blacklisted (no stream output)
    void enforceLoggingOfBlacklistMsg(bool enabled = true);

    // sets the file name for log file to 'fileName'
    void setLogFileName(const QString& fileName);

    // suppress output to stream (still log in history)
    void squelch(bool enabled = true);
    void setQuiet(bool enabled = true); // same as squelch(bool)

    // change what information is included in message string
    void toggleDateTag(bool show);
    void toggleMessageOriginTag(bool show);
    void toggleTimeTag(bool show);
    void toggleTypeTag(bool show);
    void toggleAllTags(bool show);

    // whitelisting - whitelisted classes, functions or file ignore blacklists
    template<class ... QString>
    void whitelistClassOrFunction(const QString& ... names);
    template<class ... QString>
    void whitelistFile(const QString& ... names);

    // writes the log to the file set via setLogFileName. returns true on success
    bool writeLogFile() const;
    // same as writeLogFile(); clears the log after a successful write.
    bool writeLogFileAndClear();

    // central logging function, processes log message received from Qt streams
    void processMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg);

signals:
    void newLogEntry() const;
    void messagePrinted() const;

public slots:
    void messageFromSignal(QString msg);

private:
    MessageHandler();    

    QStringList _theLog;
    QString _logfileName = QStringLiteral("ctllog.txt");

    bool _blacklistAll = false;
    bool _blacklistMsgType[5] = {};
    bool _logBlacklistedMsg = false;
    bool _showDateTag = false;
    bool _showMsgOrig = false;
    bool _showTimeTag = true;
    bool _showTypeTag = true;
    bool _squelched = false;

    QStringList _blacklistClassFct;
    QStringList _blacklistFiles;

    QStringList _whitelistClassFct;
    QStringList _whitelistFiles;

    bool isBlacklisted(QtMsgType type, const QMessageLogContext& context) const;
    bool isBlacklistedClassOrFct(const QMessageLogContext& context) const;
    bool isBlacklistedFile(const QMessageLogContext& context) const;
    bool isBlacklistedMessageType(QtMsgType type) const;

    bool isWhitelisted(const QMessageLogContext& context) const;
    bool isWhitelistedClassOrFct(const QMessageLogContext& context) const;
    bool isWhitelistedFile(const QMessageLogContext& context) const;

    QString dateTimeTag() const;
    static QString messageOriginString(const QMessageLogContext& context);
    static QString typeTag(QtMsgType type);

    void printMessage(const QString& finalMsg, QtMsgType type) const;

    static bool screenList(const QStringList& list, const char* str);
};

/*!
 * Adds all passed class and function names to the blacklist. You can pass an arbitrary number of
 * names. There is no differentiation between class and function names, ie. any class or function
 * name that matches a string passed to \a names will be blacklisted. Note that blacklist strings
 * do not need to be complete; partial matches already lead to blacklisting of all corresponding
 * entries.
 *
 * Example:
 * \code
 * MessageHandler::instance().blacklistClassOrFunction("CarmGantry");
 * // suppresses all messages coming from the CarmGantry class
 *
 * MessageHandler::instance().blacklistClassOrFunction("CarmGantry", "GenericGantry", "TubularGantry");
 * // suppresses all messages coming from the classes CarmGantry, GenericGantry, and TubularGantry
 *
 * MessageHandler::instance().blacklistClassOrFunction("Gantry");
 * // suppresses all messages coming from any class (and function) that contains "Gantry" in its name
 * \endcode
 *
 * Blacklisted messages will not be printed as console output. However, you can enforce logging of
 * these messages to the log file by enabling enforceLoggingOfBlacklistMsg(), if necessary.
 */
template<class ... QString>
void MessageHandler::blacklistClassOrFunction(const QString& ... names)
{
    _blacklistClassFct.append(QStringList{ names... });
}

/*!
 * Adds all passed file names to the blacklist. You can pass an arbitrary number of names. Note that
 * blacklist strings do not need to be complete; partial matches already lead to blacklisting of all
 * corresponding entries.
 *
 * Example:
 * \code
 * MessageHandler::instance().blacklistFile("trajectories.cpp");
 * // suppresses all messages coming from anywhere within trajectories.cpp
 *
 * MessageHandler::instance().blacklistFile("trajectories");
 * // same as above (would also suppress files with other suffix)
 *
 * // multiple input strings
 * MessageHandler::instance().blacklistFile("preparesteps.cpp", "preparationprotocols.cpp")
 * // suppresses all messages coming from the preparesteps.cpp and preparationprotocols.cpp files
 *
 * // partial matching
 * MessageHandler::instance().blacklistFile("prep");
 * // suppresses all messages coming from files that contain the "prep" substring;
 * // therefore, also suppresses preparesteps.cpp and preparationprotocols.cpp (like previous example)
 * \endcode
 *
 * Blacklisted messages will not be printed as console output. However, you can enforce logging of
 * these messages to the log file by enabling enforceLoggingOfBlacklistMsg(), if necessary.
 */
template<class ... QString>
void MessageHandler::blacklistFile(const QString& ... names)
{
    _blacklistFiles.append(QStringList{ names... });
}

/*!
 * Adds all passed class and function names to the whitelist. You can pass an arbitrary number of
 * names. There is no differentiation between class and function names, ie. any class or function
 * name that matches a string passed to \a names will be whitelisted. Note that whitelist strings
 * do not need to be complete; partial matches already lead to whitelisting of all corresponding
 * entries.
 *
 * Whitelisted messages ignore all blacklist entries, meaning they will always be printed as console
 * output.
 *
 * Whitelisting is only meaningful in combination with previous blacklisting.
 *
 * Example: suppress all message output except for messages from the CarmGantry class
 * \code
 * // blacklist everything
 * MessageHandler::instance().blacklistAll();
 *
 * // whitelist CarmGantry
 * MessageHandler::instance().whitelistClassOrFunction("CarmGantry");
 * \endcode
 *
 * Example 2: suppress all message output except for messages from any "Gantry" class
 * \code
 * // blacklist everything
 * MessageHandler::instance().blacklistAll();
 *
 * // whitelist all classes containing the "Gantry" substring
 * MessageHandler::instance().whitelistClassOrFunction("Gantry");
 * \endcode
 */
template<class ... QString>
void MessageHandler::whitelistClassOrFunction(const QString& ... names)
{
    _whitelistClassFct.append(QStringList{ names... });
}

/*!
 * Adds all passed file names to the whitelist. You can pass an arbitrary number of names. Note that
 * whitelist strings do not need to be complete; partial matches already lead to whitelisting of all
 * corresponding entries.
 *
 * Whitelisted messages ignore all blacklist entries, meaning they will always be printed as console
 * output.
 *
 * Whitelisting is only meaningful in combination with previous blacklisting.
 *
 * Example: suppress all message output except for messages from the trajectories.cpp file
 * \code
 * // blacklist everything
 * MessageHandler::instance().blacklistAll();
 *
 * // whitelist file "trajectories.cpp"
 * MessageHandler::instance().whitelistFile("trajectories.cpp");
 * \endcode
 */
template<class ... QString>
void MessageHandler::whitelistFile(const QString& ... names)
{
    _whitelistFiles.append(QStringList{ names... });
}


} // namespace CTL

#endif // CTL_MESSAGEHANDLER_H
