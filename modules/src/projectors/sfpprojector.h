#ifndef CTL_SFPPROJECTOR_H
#define CTL_SFPPROJECTOR_H

#include "acquisition/viewgeometry.h"
#include "projectors/abstractprojector.h"

namespace cl {
    class Device;
}

namespace CTL {

class SparseVoxelVolume;

namespace OCL {

class SFPProjector : public AbstractProjector
{
public:
    enum FootprintType { TR, TT, TT_Generic };
    class Settings
    {
    public:
        FootprintType footprintType = TR;
        bool useAtomicFloat = false;
    };

    SFPProjector();

    // AbstractProjector interface
    void configure(const AcquisitionSetup& setup) override;
    ProjectionData project(const VolumeData& volume) override;

    ProjectionData projectSparse(const SparseVoxelVolume& volume) override;

    Settings& settings();
private:
    static void initOpenCL();
    static size_t xWorksize(const VolumeData& volume, const cl::Device& device);
    static size_t xWorksize(const SparseVoxelVolume& volume, const cl::Device& device);

    // angle corrections
    QVector<float> determineAzimutCorrection() const;
    QVector<float> determineThetaCorrection() const;
    float phi(  uint s, const mat::ProjectionMatrix& pMat) const;
    float theta(uint t, const mat::ProjectionMatrix& pMat) const;

    void evaluatePmats() const;
    int localBufferToUse(const cl::Device& device) const;
    static size_t maxUsedWorksize(const cl::Device& device);
    int nbReqAreas(int localBufferItems) const;
    std::array<float,16> parameterArray(const VolumeData& volume,
                                        const cl::Device& device) const;
    std::array<float,16> parameterArray(const SparseVoxelVolume& volume,
                                        const cl::Device& device) const;
    void performAnisotropyCorrection(mat::Matrix<3,1> voxelSize);
    std::string programName() const;
    uint totalNbProjections() const;
    QVector<float> vectorizedPmats() const;

    QVector<float> _angleCorrAzi;        //!< angle correction azimuthal (fan)
    QVector<float> _angleCorrPol;        //!< angle correction polar (cone)
    SingleViewData::Dimensions _viewDim; //!< dimensions of a single view
    FullGeometry _pMats;                 //!< full set of projection matrices (all views and modules)
    Settings _settings;                  //!< settings for the projector
};

} // namespace OCL
} // namespace CTL

#endif // CTL_SFPPROJECTOR_H
