#ifndef CTL_RAYCASTER_H
#define CTL_RAYCASTER_H

#define __CL_ENABLE_EXCEPTIONS
#define CL_TARGET_OPENCL_VERSION 120
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#if defined(__APPLE__) || defined(__MACOSX)
  #include "OpenCL/cl.hpp"
#else
  #if defined __has_include && __has_include("CL/cl2.hpp")
    #define CL_HPP_ENABLE_EXCEPTIONS
    #define CL_HPP_TARGET_OPENCL_VERSION 120
    #define CL_HPP_MINIMUM_OPENCL_VERSION 120
    #define CL_HPP_CL_1_2_DEFAULT_BUILD
    #define CL_HPP_ENABLE_SIZE_T_COMPATIBILITY
    #define CL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY
    #include <CL/cl2.hpp>
  #else
    #include <CL/cl.hpp>
  #endif
#endif

#include "mat/matrix_types.h"

namespace CTL {
namespace OCL {
namespace external {

class RayCaster
{
public:
    RayCaster();
    std::vector<float> project(const std::vector<ProjectionMatrix>& Pmats,
                               const std::vector<float>& volume);

    void setDetectorSize(uint nbRows, uint nbColumns);
    void setIncrement(float incrementMM);
    void setVolumeOffset(const float (&offset)[3]);
    void setVolumeInfo(const uint (&nbVoxel)[3], const float (&voxSize)[3]);

private:
    cl::Context context;
    cl::Program program;
    std::vector<cl::Device> device;

    cl_uint detectorColumns;
    cl_uint detectorRows;
    cl_float increment_mm;
    cl::size_t<3> volDim;
    cl_float3 volOffset;
    cl_float3 voxelSize;

    void initOpenCL();

    cl_float3 volumeCorner() const;
};

} // namespace external
} // namespace OCL
} // namespace CTL

#endif // CTL_RAYCASTER_H
