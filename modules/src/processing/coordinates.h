#ifndef CTL_COORDINATES_H
#define CTL_COORDINATES_H

#include <algorithm>
#include <vector>

typedef unsigned int uint;

namespace CTL {

struct Generic2DCoord
{
    Generic2DCoord() = default;
    Generic2DCoord(float coord1, float coord2) : data{ coord1, coord2 } {}

    float& coord1() { return data[0]; }
    float& coord2() { return data[1]; }
    const float& coord1() const { return data[0]; }
    const float& coord2() const { return data[1]; }

    float data[2];
};

struct Generic3DCoord
{
    Generic3DCoord() = default;
    Generic3DCoord(float coord1, float coord2, float coord3) : data{ coord1, coord2, coord3 } {}

    float& coord1() { return data[0]; }
    float& coord2() { return data[1]; }
    float& coord3() { return data[2]; }
    const float& coord1() const { return data[0]; }
    const float& coord2() const { return data[1]; }
    const float& coord3() const { return data[2]; }

    float data[3];
};

struct Generic2DIndex
{
    Generic2DIndex() = default;
    Generic2DIndex(uint idx1, uint idx2) : data{ idx1, idx2 } {}

    uint& idx1() { return data[0]; }
    uint& idx2() { return data[1]; }
    const uint& idx1() const { return data[0]; }
    const uint& idx2() const { return data[1]; }

    uint data[2];
};

struct Generic3DIndex
{
    Generic3DIndex() = default;
    Generic3DIndex(uint idx1, uint idx2, uint idx3) : data{ idx1, idx2, idx3 } {}

    uint& idx1() { return data[0]; }
    uint& idx2() { return data[1]; }
    uint& idx3() { return data[2]; }
    const uint& idx1() const { return data[0]; }
    const uint& idx2() const { return data[1]; }
    const uint& idx3() const { return data[2]; }

    uint data[3];
};

} // namespace CTL

#endif // CTL_COORDINATES_H
