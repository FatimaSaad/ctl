#include <QApplication>

#include "ctl.h"
#include "ctl_ocl.h"
#include "ctl_qtgui.h"

using namespace CTL;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    auto vol = CTL::VoxelVolume<float>::cylinderZ(50.f, 60.f, 0.5f, 0.03f);
    vol.setVolumeOffset(0.f, 0.f, 30.f);

    //defining the setup
    const auto nbViews = 100u;
    const auto sourceToIsocenter = 785.;
    const auto startAngle = 10._deg;
    const auto smallAngle = 15._deg;
    const auto largeAngle = 23._deg;

    auto setup = AcquisitionSetup(makeCTSystem<blueprints::GenericCarmCT>(DetectorBinning::Binning4x4), nbViews);
    setup.applyPreparationProtocol(CTL::protocols::TomosynthesisEllipticalTrajectory(
                                       sourceToIsocenter, startAngle, smallAngle, largeAngle, true));
    CTL::gui::plot(setup);

    // projecting the volume
    auto projections = OCL::RayCasterProjector{}.configureAndProject(setup, vol);
    CTL::gui::plot(projections);

    return a.exec();
}
